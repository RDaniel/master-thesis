import pandas as pd
from pandas import DataFrame
from sklearn.manifold import TSNE

from graph_utils import *
from utils import *


def extract_and_visualize_attention(layers, dataset):
    for layer_index, layer in enumerate(layers):
        alpha = layer.alpha
        indices = layer.aggr_kwargs['index']

        attentions = extract_node_attentions(alpha, indices)
        for head, nodes_attention in attentions.items():
            title = dataset + ' - Sloj: {}, Glava: {}'.format(layer_index + 1, head + 1)
            heads_entropy = calculate_nodes_entropy(nodes_attention)
            draw_histogram(heads_entropy, title=title)
            print(title + ' AVG Entropy : {}'.format(sum(heads_entropy) / len(heads_entropy)))

    uniform = get_uniform_attentions(nodes_attention)
    heads_entropy = calculate_nodes_entropy(uniform)
    draw_histogram(heads_entropy, title=dataset + ' - Uniformna pažnja')
    print('Uniform attention AVG entropy : {}'.format(sum(heads_entropy) / len(heads_entropy)))


def visualize_learning_process(model_name, values, value_name, variable_name, iteration_multiplier=1, legend='brief'):
    size = len(list(values.values())[0])
    x = [element * iteration_multiplier for element in list(range(size))]
    values['Iteracija'] = x

    values = DataFrame(values)
    values = values.melt('Iteracija', var_name=variable_name, value_name=value_name)

    draw_lineplot('Iteracija', value_name, variable_name, values, model_name, legend=legend)


def visualize_depth_effect(dataset_results, max_depth, model):
    for dataset, results in dataset_results.items():
        results['Broj slojeva'] = list(range(1, max_depth))
        results = DataFrame(results)
        results = results.melt('Broj slojeva', var_name='Podatci', value_name='Točnost')

        draw_lineplot('Broj slojeva', 'Točnost', 'Podatci', results, model + ' - ' + dataset)


def visualize_data_embedding(embeddings, y):
    tsne = TSNE(n_components=2)

    for name, embedding in embeddings.items():
        embedding = tsne.fit_transform(embedding)
        embedding = pd.DataFrame(embedding, columns=['x', 'y'])
        embedding['Razred'] = y
        embedding = embedding.astype({'Razred': 'int32'})

        draw_scatter_plot('x', 'y', 'Razred', embedding, name, colors_number=max(y) + 1)


def visualize_sparsity_pattern(graph):
    draw_spy_plot(graph)


def calculate_graph_statistics(graph):
    graph = graph.copy()
    nx_graph = nx.from_numpy_matrix(graph).to_undirected()

    statistics = {}

    d_max, d_min, d_mean = degrees(graph)

    lcc = statistics_LCC(graph)
    statistics['LCC'] = lcc.shape[0]
    statistics['d_max'] = d_max
    statistics['d_min'] = d_min
    statistics['d'] = d_mean
    statistics['wedge_count'] = wedge_count(graph)
    statistics['claw_count'] = claw_count(graph)
    statistics['triangle_count'] = triangle_count(graph)
    statistics['gini'] = gini(graph)
    statistics['rel_edge_distr_entropy'] = edge_distribution_entropy(graph)
    statistics['assortativity'] = nx.degree_assortativity_coefficient(nx_graph)
    statistics['clustering_coefficient'] = 3 * statistics['triangle_count'] / statistics['claw_count']
    statistics['n_components'] = connected_components(graph)[0]

    return statistics

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from scipy.stats import entropy

sns.set(palette='colorblind', color_codes=True)
sns.set_context(font_scale=1.25, rc={"lines.linewidth": 2})


def extract_node_attentions(attention, indices):
    heads_number = len(attention.data[0])
    heads_dict = {}

    for head in range(heads_number):
        nodes_dict = {}
        for i, index in enumerate(indices):
            index = int(index)
            if index not in nodes_dict:
                nodes_dict[index] = []

            nodes_dict[index].append(float(attention.data[i][head]))

        heads_dict[head] = nodes_dict

    return heads_dict


def calculate_nodes_entropy(nodes):
    result = []
    for node, values in nodes.items():
        result.append(entropy(values, base=2))

    return result


def get_uniform_attentions(attentions):
    nodes_dict = {}

    for node, values in attentions.items():
        uniform_values = [1.0 / len(values)]
        nodes_dict[node] = np.repeat(uniform_values, len(values))

    return nodes_dict


def draw_histogram(x, title='Attention histogram', bins=6):
    x = [i for i in x if i < 5]
    ax = sns.distplot(
        x,
        bins=bins,
        kde=False,
        hist_kws={"range": [0, 5]},
        label='Entropija',
    )
    ax.set(title=title, xlabel='Entropija', ylabel='Broj čvorova')
    plt.show()


def draw_lineplot(x, y, hue, data, title, legend='brief'):
    ax = sns.lineplot(x=x, y=y, hue=hue, markers=True, data=data, legend=legend)
    ax.set(title=title)
    plt.show()


def draw_scatter_plot(x, y, hue, data, title, colors_number):
    palette = sns.color_palette('colorblind', n_colors=colors_number)
    ax = sns.scatterplot(x=x, y=y, palette=palette, hue=hue, data=data, legend='full')
    ax.set(title=title)
    plt.show()


def draw_spy_plot(data):
    plt.spy(data, markersize=0.2)
    plt.show()

import time

import torch
import torch.nn.functional as F
from sklearn.metrics import accuracy_score

from validation import batch_validation, mask_accuracy


class TransductiveTrainer:

    def __init__(self, model, loss=F.nll_loss, learning_rate=1e-3, weight_decay=5e-4):
        self.model = model
        self.loss = loss

        self.optimizer = torch.optim.Adam(params=self.model.parameters(), lr=learning_rate, weight_decay=weight_decay)

    def train(self, data, number_of_epochs, max_patience=30):
        best_val_acc = 0
        patience = max_patience

        losses = []
        train_list = []
        val_list = []

        for epoch in range(1, number_of_epochs):
            self.model.train()
            self.optimizer.zero_grad()
            loss = self.loss(self.model(data)[data.train_mask], data.y[data.train_mask])
            loss.backward()
            self.optimizer.step()

            train_acc, val_acc = self._validate(data)
            if val_acc > best_val_acc:
                best_val_acc = val_acc
                patience = max_patience
            else:
                patience -= 1

            if patience == 0:
                break

            losses.append(float(loss.item()))
            train_list.append(train_acc)
            val_list.append(val_acc)

            template = 'Epoch: {}, Loss: {:.5}, Train: {:.3}, Best val: {:.3}'
            print(template.format(epoch, loss.item(), train_acc, best_val_acc))

        return losses, train_list, val_list

    @torch.no_grad()
    def _validate(self, data):
        self.model.eval()
        accuracies = []

        for mask in [data.train_mask, data.val_mask]:
            accuracy = mask_accuracy(self.model, data, mask)
            accuracies.append(accuracy)

        return accuracies


class InductiveTrainer:

    def __init__(self, model, loss=F.nll_loss, learning_rate=1e-3, weight_decay=5e-4):
        self.model = model
        self.loss = loss

        self.optimizer = torch.optim.Adam(params=self.model.parameters(), lr=learning_rate, weight_decay=weight_decay)

    def train(self, train_data, val_data, number_of_epochs, max_patience=30):
        best_val_acc = 0
        patience = max_patience

        losses = []
        train_list = []
        val_list = []

        starting_time = time.time()

        for epoch in range(1, number_of_epochs):
            total_loss = 0
            for batch in train_data:
                self.model.train()
                self.optimizer.zero_grad()

                loss = self.loss(self.model(batch), batch.y)
                loss.backward()
                self.optimizer.step()
                total_loss += (loss.item() / batch.num_graphs)

            train_acc, val_acc = self._validate(train_data, val_data)

            losses.append(float(total_loss))
            train_list.append(train_acc)
            val_list.append(val_acc)

            if val_acc > best_val_acc:
                best_val_acc = val_acc
                patience = max_patience
            else:
                patience -= 1

            if patience == 0:
                break

            running = time.time() - starting_time
            print('Epoch: {}, Loss: {}, Val accuracy: {:.4f} Running: {}'.format(epoch, total_loss, best_val_acc,
                                                                                 running))
        return losses, train_list, val_list

    @torch.no_grad()
    def _validate(self, train_data, val_data):
        self.model.eval()
        train_score = batch_validation(self.model, train_data, accuracy_score)
        test_score = batch_validation(self.model, val_data, accuracy_score)
        return train_score, test_score

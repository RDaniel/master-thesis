from dataset import get_polaroid_dataset
from graph_attention import GraphAttentionNetwork
from graph_convolution import GraphConvolutionNetwork
from postprocessing import visualize_depth_effect
from trainer import TransductiveTrainer
from validation import mask_accuracy

POLAROID_DATASETS = ['Cora', 'CiteSeer', 'PubMed']
MAX_DEPTH = 10

dataset_results = {}

for depth in range(1, MAX_DEPTH):
    for dataset_name in POLAROID_DATASETS:
        if dataset_name not in dataset_results:
            dataset_results[dataset_name] = {}
            dataset_results[dataset_name]['treniranje'] = []
            dataset_results[dataset_name]['testiranje'] = []

        dataset = get_polaroid_dataset(dataset_name)
        data = dataset.data

        layer_size = [dataset.num_features]
        heads = [4]
        for i in range(depth - 1):
            layer_size.append(32)
            heads.append(4)
        layer_size.append(dataset.num_classes)

        model = GraphConvolutionNetwork(layers_size=layer_size)
        trainer = TransductiveTrainer(model, learning_rate=0.0005, weight_decay=5e-5)
        _, train_accuracy, _ = trainer.train(data, number_of_epochs=150, max_patience=125)

        model.eval()
        accuracy = mask_accuracy(model, data, data.test_mask)

        dataset_results[dataset_name]['treniranje'].append(train_accuracy[-1])
        dataset_results[dataset_name]['testiranje'].append(accuracy)

visualize_depth_effect(dataset_results, MAX_DEPTH, 'GCN')

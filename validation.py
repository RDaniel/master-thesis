import torch
from sklearn.metrics import f1_score


def batch_validation(model, data_loader, metric):
    labels = []
    predictions = []
    for batch in data_loader:
        logits = model(batch, logits=False)
        pred = logits.max(1)[1]

        predictions.append(pred)
        labels.append(batch.y)

    y, pred = torch.cat(labels, dim=0).numpy(), torch.cat(predictions, dim=0).numpy()
    return metric(y, pred)


def batch_multiclass_validation(model, data_loader, metric, average='micro'):
    labels = []
    predictions = []

    for batch in data_loader:
        logits = model(batch, logits=True)
        pred = (logits > 0).float()

        predictions.append(pred)
        labels.append(batch.y)

    y, pred = torch.cat(labels, dim=0).numpy(), torch.cat(predictions, dim=0).numpy()

    return metric(y, pred, average=average)


def mask_accuracy(model, data, mask):
    logits = model(data)
    pred = logits[mask].max(1)[1]

    return pred.eq(data.y[mask]).sum().item() / mask.sum().item()


def mask_f1(model, data, mask):
    logits = model(data)
    pred = logits[mask].max(1)[1]

    return f1_score(pred, data.y[mask], average='macro')

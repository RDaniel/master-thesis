from sklearn.metrics import f1_score, accuracy_score
from torch_geometric.data import DataLoader

import postprocessing as pp
from dataset import get_polaroid_dataset
from dataset import get_proteins_dataset
from graph_convolution import GraphConvolutionNetwork, GraphConvolutionPoolingNetwork
from trainer import InductiveTrainer
from trainer import TransductiveTrainer
from validation import batch_validation, mask_f1
from validation import mask_accuracy

POLAROID_DATASETS = ['Cora', 'CiteSeer', 'PubMed']

losses = {}
accuracies = {}

for dataset_name in POLAROID_DATASETS:
    print('Current dataset: ' + dataset_name)
    dataset = get_polaroid_dataset(dataset_name)
    data = dataset.data

    model = GraphConvolutionNetwork(layers_size=[dataset.num_features, 32, dataset.num_classes])
    trainer = TransductiveTrainer(model, learning_rate=0.001, weight_decay=5e-5)
    loss, train_accuracy, _ = trainer.train(data, number_of_epochs=100, max_patience=100)

    losses[dataset_name] = loss
    accuracies[dataset_name] = train_accuracy

    model.eval()
    accuracy = mask_accuracy(model, data, data.test_mask)
    f1 = mask_f1(model, data, data.test_mask)
    print("Test Accuracy: {}".format(accuracy))
    print("Test F1: {}".format(f1))

train, test, val = get_proteins_dataset()

test_loader = DataLoader(test, batch_size=32)
train_loader = DataLoader(train, batch_size=32)
val_loader = DataLoader(val, batch_size=32)

model = GraphConvolutionPoolingNetwork(train.num_features, train.num_classes)
trainer = InductiveTrainer(model, learning_rate=0.0005, weight_decay=5e-6)
loss, train_accuracy, _ = trainer.train(train_loader, val_loader, number_of_epochs=100, max_patience=30)

losses['Proteins'] = loss
accuracies['Proteins'] = train_accuracy

model.eval()
result = batch_validation(model, test_loader, accuracy_score)
print("Test Accuracy : {}".format(result))

result = batch_validation(model, test_loader, f1_score)
print("Test F1-score : {}".format(result))

pp.visualize_learning_process('GCN', losses, 'Gubitak', 'Skup podataka')
pp.visualize_learning_process('GCN', accuracies, 'Točnost', 'Skup podataka')

import time

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import grad

import graph_utils as utils


def get_xavier_weights(*size):
    weights = torch.zeros(size=size, dtype=torch.float32)
    torch.nn.init.xavier_uniform_(weights.data)

    return weights


def make_gaussian_noise(shape):
    return torch.randn(shape)


class NetGAN:
    GENERATOR_PATH = 'pretrained_models/netgan_generator.pt'
    DISCRIMINATOR_PATH = 'pretrained_models/netgan_discriminator.pt'

    def __init__(self, N, rw_len, data_loader, generator_size=40, discriminator_size=30, down_generator_size=128,
                 down_discriminator_size=128, batch_size=128, noise_dimension=16, learning_rate=0.0003, W_penalty=10,
                 discriminator_iterations=3, generator_l2_penalty=1e-7, discriminator_l2_penalty=5e-5, restore=False):
        self.N = N
        self.rw_len = rw_len
        self.data_loader = data_loader
        self.batch_size = batch_size
        self.noise_dimension = noise_dimension

        self.learning_rate = learning_rate
        self.discriminator_iterations = discriminator_iterations

        self.W_penalty = W_penalty
        self.generator_l2_penalty = generator_l2_penalty
        self.discriminator_l2_penalty = discriminator_l2_penalty

        self.generator = Generator(self.N, self.rw_len, self.noise_dimension, generator_size, down_generator_size)
        self.discriminator = Discriminator(self.N, self.rw_len, discriminator_size, down_discriminator_size)

        if restore:
            self.generator.load_state_dict(torch.load(self.GENERATOR_PATH, map_location=torch.device('cpu')))
            self.discriminator.load_state_dict(torch.load(self.DISCRIMINATOR_PATH, map_location=torch.device('cpu')))

    def train(self, A_orig, max_iterations=10000, eval_transitions=2e6,
              transitions_per_iter=100000, max_patience=20, eval_every=400):
        val_roc = []
        val_ap = []
        edge_overlaps = []

        patience = max_patience
        best_performance = 0.0

        transitions_per_walk = self.rw_len - 1
        sample_many_count = int(np.round(transitions_per_iter / transitions_per_walk))
        n_eval_walks = eval_transitions / transitions_per_walk
        n_eval_iterations = int(np.round(n_eval_walks / sample_many_count))

        starting_time = time.time()

        data_iterator = iter(self.data_loader)

        betas = (0.5, 0.9)
        generator_optimizer = torch.optim.Adam(self.generator.parameters(),
                                               lr=self.learning_rate,
                                               weight_decay=self.generator_l2_penalty,
                                               betas=betas)
        discriminator_optimizer = torch.optim.Adam(self.discriminator.parameters(),
                                                   lr=self.learning_rate,
                                                   weight_decay=self.discriminator_l2_penalty,
                                                   betas=betas)

        for iteration in range(max_iterations):
            if iteration > 0 and iteration % 10 == 0:
                print("Current iteration: {}  Running time: {} seconds".format(iteration, time.time() - starting_time))

            for _ in range(self.discriminator_iterations):
                self._discriminator_train_step(data_iterator, discriminator_optimizer)

            self._generator_train_step(generator_optimizer)

            if iteration > 0 and iteration % eval_every == 0:
                print("Evaluating model!")
                graph, _ = self.generate_sample_graph(A_orig, sample_many_count, n_eval_iterations)
                edge_overlap, performance = self._evaluate_model(A_orig, graph)

                edge_overlaps.append(edge_overlap / A_orig.sum())
                val_roc.append(performance[0])
                val_ap.append(performance[1])

                current_score = val_roc[-1] + val_ap[-1]
                if current_score > best_performance:
                    best_performance = current_score
                    print("New best model: {}".format(best_performance))
                    patience = max_patience
                else:
                    patience -= 1

                if patience == 0:
                    print("Early stopping after {} iterations".format(iteration))
                    break

                print("Iter {:<6} Val ROC {:.3f}, AP: {:.3f}, EO {:.3f}".format(
                    iteration,
                    val_roc[-1],
                    val_ap[-1],
                    edge_overlap / A_orig.sum())
                )

        return {'edge_overlaps': edge_overlaps, 'val_roc': val_roc, 'val_ap': val_ap}

    def save(self):
        torch.save(self.generator.state_dict(), self.GENERATOR_PATH)
        torch.save(self.discriminator.state_dict(), self.DISCRIMINATOR_PATH)

    def _generator_train_step(self, generator_optimizer):
        fake_walks = self.generator(self.batch_size)
        fake_predictions = self.discriminator(fake_walks)

        generator_optimizer.zero_grad()
        generator_loss = - torch.mean(fake_predictions)
        generator_loss.backward()
        generator_optimizer.step()

    def _discriminator_train_step(self, data_iterator, discriminator_optimizer):
        fake_walks = self.generator(self.batch_size).detach()
        real_walks = next(data_iterator)
        real_walks = F.one_hot(real_walks, num_classes=self.N).type(torch.FloatTensor)

        fake_predictions = self.discriminator(fake_walks)
        real_predictions = self.discriminator(real_walks)

        grad_penalty = self._get_gradient_penalty(fake_walks, real_walks)

        discriminator_optimizer.zero_grad()
        discriminator_loss = torch.mean(fake_predictions) - torch.mean(real_predictions) + grad_penalty
        discriminator_loss.backward()
        discriminator_optimizer.step()

    def _get_gradient_penalty(self, fake_walks, real_walks):
        alpha = torch.rand(size=(self.batch_size, 1, 1))
        differences = fake_walks - real_walks
        interpolation = real_walks + alpha * differences
        interpolation.requires_grad_()

        lipschitz_grad = grad(
            outputs=self.discriminator(interpolation).sum(),
            inputs=interpolation,
            create_graph=True,
            retain_graph=True)[0]

        grad_norm = torch.sqrt(torch.sum(lipschitz_grad ** 2, dim=[1, 2]))
        grad_norm = torch.mean((grad_norm - 1.0) ** 2)
        grad_penalty = self.W_penalty * grad_norm

        return grad_penalty

    def generate_sample_graph(self, A_orig, n_eval_iterations, sample_many_count):
        samples = []
        with torch.no_grad():
            for i in range(n_eval_iterations):
                sample = self.generator(sample_many_count)
                sample = np.argmax(sample, axis=-1)
                samples.append(sample)

        samples = np.array(torch.stack(samples, dim=0)).reshape([-1, self.rw_len])
        gr = utils.score_matrix_from_random_walks(samples, self.N)
        gr = gr.tocsr()
        _graph = utils.graph_from_scores(gr, A_orig.sum())

        return _graph, gr

    def _evaluate_model(self, A_orig, graph):
        edge_overlap = utils.edge_overlap(A_orig.toarray(), graph)
        return edge_overlap


class Generator(nn.Module):

    def __init__(self, N, walk_size, noise_dim, size, down_size):
        super(Generator, self).__init__()

        self.N = N
        self.size = size
        self.walk_size = walk_size
        self.down_size = down_size
        self.noise_dim = noise_dim

        self.W_down = torch.nn.Parameter(get_xavier_weights(self.N, self.down_size))
        self.W_up = torch.nn.Parameter(get_xavier_weights(size, self.N))
        self.b_W_up = torch.nn.Parameter(get_xavier_weights(1, self.N))

        self.intermediate = nn.Linear(self.noise_dim, size)
        self.h = nn.Linear(size, size)
        self.c = nn.Linear(size, size)

        self.lstm = nn.LSTMCell(self.down_size, size)

    def forward(self, batch_size, noise=None):
        if noise is None:
            noise = make_gaussian_noise((batch_size, self.noise_dim))

        intermediate = self.intermediate(noise)
        h = torch.tanh(self.h(intermediate))
        c = torch.tanh(self.c(intermediate))

        state = (h, c)
        inputs = torch.zeros(batch_size, self.down_size)
        outputs = []

        for i in range(self.walk_size):
            state = self.lstm(inputs, state)
            output = state[0]

            output_projection = torch.matmul(output, self.W_up) + self.b_W_up
            output = F.gumbel_softmax(output_projection, hard=True, tau=1.5)
            outputs.append(output)

            inputs = torch.matmul(output, self.W_down)

        outputs = torch.stack(outputs, dim=1)
        return outputs


class Discriminator(nn.Module):

    def __init__(self, N, walk_size, size, down_size):
        super(Discriminator, self).__init__()

        self.N = N
        self.size = size
        self.walk_size = walk_size
        self.down_size = down_size

        self.W_down = torch.nn.Parameter(get_xavier_weights(self.N, self.down_size))
        self.W_up = torch.nn.Parameter(get_xavier_weights(size, self.N))

        self.lstm = nn.LSTMCell(self.down_size, size)

        self.output_projection = nn.Linear(self.size, 1)

    def forward(self, inputs):
        n_samples = inputs.size()[0]
        h = torch.zeros(n_samples, self.size)
        c = torch.zeros(n_samples, self.size)
        state = (h, c)

        inputs = inputs.view(-1, self.N)
        inputs = torch.matmul(inputs, self.W_down)
        inputs = inputs.view(-1, self.walk_size, self.down_size)

        for step in inputs.unbind(dim=1):
            state = self.lstm(step, state)

        output = state[0]
        output = self.output_projection(output)

        return output

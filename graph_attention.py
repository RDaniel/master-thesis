import torch
import torch.nn.functional as F
from torch_geometric.nn import GATConv
from torch_geometric.nn import global_mean_pool, global_max_pool


class GraphAttentionNetwork(torch.nn.Module):

    def __init__(self, layers_size, heads):
        super(GraphAttentionNetwork, self).__init__()

        self.layers = torch.nn.ModuleList()

        layers_number = len(layers_size) - 1
        for i in range(layers_number):
            heads_multiplier = 1 if i == 0 else heads[i - 1]
            input_size = layers_size[i] * heads_multiplier
            concat = False if i == layers_number - 1 else True
            conv = GATConv(input_size, layers_size[i + 1], heads=heads[i], concat=concat)
            self.layers.append(conv)

    def forward(self, data, logits=False):
        x, edge_index, edge_weight = data.x, data.edge_index, data.edge_attr

        for layer in self.layers:
            x = F.dropout(x, p=0.6, training=self.training)
            x = layer(x, edge_index, edge_weight)

            if layer is not self.layers[-1]:
                x = F.elu(x)

        if logits:
            return x
        else:
            return F.log_softmax(x, dim=1)


class GraphAttentionPoolingNetwork(torch.nn.Module):

    def __init__(self, input, output, heads=4, hidden=32):
        super(GraphAttentionPoolingNetwork, self).__init__()

        self.conv1 = GATConv(input, hidden, heads=heads, concat=False)
        self.conv2 = GATConv(hidden, hidden, heads=heads, concat=False)
        self.conv3 = GATConv(hidden, hidden, heads=heads, concat=False)

        self.layers = [self.conv1, self.conv2, self.conv3]

        self.lin1 = torch.nn.Linear(hidden * 2 * 3, hidden)
        self.lin2 = torch.nn.Linear(hidden, 128)
        self.lin3 = torch.nn.Linear(128, output)

    def forward(self, data, logits=False):
        x, edge_index, batch = data.x, data.edge_index, data.batch

        x = F.relu(self.conv1(x, edge_index))
        x1 = torch.cat([global_mean_pool(x, batch), global_max_pool(x, batch)], dim=1)

        x = F.relu(self.conv2(x, edge_index))
        x2 = torch.cat([global_mean_pool(x, batch), global_max_pool(x, batch)], dim=1)

        x = F.relu(self.conv3(x, edge_index))
        x3 = torch.cat([global_mean_pool(x, batch), global_max_pool(x, batch)], dim=1)

        x = torch.cat([x1, x2, x3], dim=1)

        x = F.relu(self.lin1(x))
        x = F.dropout(x, p=0.5, training=self.training)
        x = F.relu(self.lin2(x))
        x = self.lin3(x)

        if logits:
            return x
        else:
            return F.log_softmax(x, dim=-1)

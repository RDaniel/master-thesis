import os.path as osp

import torch_geometric.transforms as T
from torch.utils.data import IterableDataset
from torch_geometric.datasets import Planetoid, PPI, TUDataset

import graph_utils as utils


class RandomWalkDataset(IterableDataset):

    def __init__(self, generator):
        self.generator = generator

    def __iter__(self):
        return self.generator.generate()


def get_polaroid_dataset(dataset, transform=T.NormalizeFeatures()):
    path = osp.join('data', dataset)
    return Planetoid(path, dataset, transform=transform)


def get_protein_interaction_dataset(transform=T.NormalizeFeatures()):
    path = osp.join('data', 'PPI')

    train_dataset = PPI(path, split='train', transform=transform)
    val_dataset = PPI(path, split='val', transform=transform)
    test_dataset = PPI(path, split='test', transform=transform)

    return train_dataset, test_dataset, val_dataset


def get_proteins_dataset(transform=T.NormalizeFeatures(), shuffle=True):
    dataset_name = 'PROTEINS'
    path = osp.join('data', 'TUD')
    dataset = TUDataset(path, dataset_name, transform=transform)
    if shuffle:
        dataset = dataset.shuffle()

    size = len(dataset)

    train = dataset[:int(size * 0.7)]
    test = dataset[int(size * 0.7):int(size * 0.7) + int(size * 0.2)]
    val = dataset[int(size * 0.7) + int(size * 0.2):]

    return train, test, val


def get_cora_graph():
    _A_obs, _X_obs, _z_obs = utils.load_npz('data/cora_ml.npz')
    _A_obs = _A_obs + _A_obs.T
    _A_obs[_A_obs > 1] = 1
    lcc = utils.largest_connected_components(_A_obs)
    _A_obs = _A_obs[lcc, :][:, lcc]
    return _A_obs

from torch.utils.data import DataLoader

import graph_utils as utils
from dataset import RandomWalkDataset
from dataset import get_cora_graph
from netgan import NetGAN
from postprocessing import visualize_sparsity_pattern, calculate_graph_statistics

graph = get_cora_graph()
_N = graph.shape[0]

rw_len = 16
batch_size = 128

walk_generator = utils.RandomWalkGenerator(graph, rw_len)
dataset = RandomWalkDataset(walk_generator)
data_loader = DataLoader(dataset, batch_size=batch_size)

netgan = NetGAN(_N, rw_len, data_loader, restore=False)
netgan.train(graph)
print("Generating graph")
generated_graph, score_matrix = netgan.generate_sample_graph(graph, 30, 7500)

visualize_sparsity_pattern(graph)
visualize_sparsity_pattern(generated_graph)

print(calculate_graph_statistics(graph.toarray()))
print(calculate_graph_statistics(generated_graph))

import matplotlib.pyplot as plt
import networkx as nx
from torch_geometric.utils import to_networkx

from dataset import get_proteins_dataset

train, test, val = get_proteins_dataset(shuffle=False)

small_graph_examples = [2, 4, 9, 750, 763, 764]

for graph_index in small_graph_examples:
    graph = train[graph_index]

    node_attrs = {}
    for i in range(graph.x.shape[0]):
        attribute = str(list(map(int, graph.x[i].tolist())))
        attribute = attribute.replace('[', '').replace(',', '').replace(']', '')
        node_attrs[i] = attribute
    network_graph = to_networkx(graph)
    nx.set_node_attributes(network_graph, node_attrs, 'x')

    ax = plt.gca()
    node_attrs = nx.get_node_attributes(network_graph, 'x')
    nx.draw(network_graph, node_color='#65B9F3', edge_color='#C4BBBA', labels=node_attrs, ax=ax, with_labels=True)
    ax.set(title='Razred: ' + str(int(graph.y[0])))
    plt.show(block=False)

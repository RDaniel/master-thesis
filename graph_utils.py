import networkx as nx
import numpy as np
import scipy.sparse as sp
from numba import jit
from scipy.sparse.csgraph import connected_components

"""
Some of the methods in this file were used from the original NetGAN implementation, which can be found
on https://github.com/danielzuegner/netgan.
"""


class RandomWalkGenerator:
    """
        Random walk generator using biased-order random walk sampling strategy described in Grover and Leskovec (2016).
    """

    def __init__(self, adj, rw_len, p=1, q=1, batch_size=1):
        self.adj = adj
        self.rw_len = rw_len
        self.p = p
        self.q = q
        self.edges = np.array(self.adj.nonzero()).T
        self.node_ixs = np.unique(self.edges[:, 0], return_index=True)[1]
        self.batch_size = batch_size

    def generate(self):
        while True:
            yield random_walk(self.edges, self.node_ixs, self.rw_len, self.p, self.q, self.batch_size)


@jit(nopython=True)
def random_walk(edges, node_ixs, rw_len, p=1, q=1, n_walks=1):
    N = len(node_ixs)

    walk = []
    prev_nbs = None

    for w in range(n_walks):
        source_node = np.random.choice(N)
        walk.append(source_node)
        for it in range(rw_len - 1):

            if walk[-1] == N - 1:
                nbs = edges[node_ixs[walk[-1]]::, 1]
            else:
                nbs = edges[node_ixs[walk[-1]]:node_ixs[walk[-1] + 1], 1]

            if it == 0:
                walk.append(np.random.choice(nbs))
                prev_nbs = set(nbs)
                continue

            is_dist_1 = []
            for n in nbs:
                is_dist_1.append(int(n in set(prev_nbs)))

            is_dist_1_np = np.array(is_dist_1)
            is_dist_0 = nbs == walk[-2]
            is_dist_2 = 1 - is_dist_1_np - is_dist_0

            alpha_pq = is_dist_0 / p + is_dist_1_np + is_dist_2 / q
            alpha_pq_norm = alpha_pq / np.sum(alpha_pq)
            rdm_num = np.random.rand()
            cumsum = np.cumsum(alpha_pq_norm)
            nxt = nbs[np.sum(1 - (cumsum > rdm_num))]
            walk.append(nxt)
            prev_nbs = set(nbs)

    return np.array(walk)


def load_npz(file_name):
    if not file_name.endswith('.npz'):
        file_name += '.npz'
    with np.load(file_name, allow_pickle=True) as loader:
        loader = dict(loader)['arr_0'].item()
        adj_matrix = sp.csr_matrix((loader['adj_data'], loader['adj_indices'], loader['adj_indptr']),
                                   shape=loader['adj_shape'])

        if 'attr_data' in loader:
            attr_matrix = sp.csr_matrix((loader['attr_data'], loader['attr_indices'],
                                         loader['attr_indptr']), shape=loader['attr_shape'])
        else:
            attr_matrix = None

        labels = loader.get('labels')

    return adj_matrix, attr_matrix, labels


def edges_to_sparse(edges, N, values=None):
    if values is None:
        values = np.ones(edges.shape[0])

    return sp.coo_matrix((values, (edges[:, 0], edges[:, 1])), shape=(N, N)).tocsr()


def largest_connected_components(adj, n_components=1):
    _, component_indices = connected_components(adj)
    component_sizes = np.bincount(component_indices)
    components_to_keep = np.argsort(component_sizes)[::-1][:n_components]
    nodes_to_keep = [idx for (idx, component) in enumerate(component_indices) if component in components_to_keep]
    return nodes_to_keep


def edge_overlap(A, B):
    return ((A == B) & (A == 1)).sum()


def graph_from_scores(scores, n_edges):
    if len(scores.nonzero()[0]) < n_edges:
        return symmetric(scores) > 0

    target_g = np.zeros(scores.shape)  # initialize target graph
    scores_int = scores.toarray().copy()  # internal copy of the scores matrix
    scores_int[np.diag_indices_from(scores_int)] = 0  # set diagonal to zero
    degrees_int = scores_int.sum(0)  # The row sum over the scores.

    N = scores.shape[0]

    for n in np.random.choice(N, replace=False, size=N):  # Iterate the nodes in random order

        row = scores_int[n, :].copy()
        if row.sum() == 0:
            continue

        probs = row / row.sum()

        target = np.random.choice(N, p=probs)
        target_g[n, target] = 1
        target_g[target, n] = 1

    diff = np.round((n_edges - target_g.sum()) / 2)
    if diff > 0:
        triu = np.triu(scores_int)
        triu[target_g > 0] = 0
        triu[np.diag_indices_from(scores_int)] = 0
        triu = triu / triu.sum()

        triu_ixs = np.triu_indices_from(scores_int)
        extra_edges = np.random.choice(triu_ixs[0].shape[0], replace=False, p=triu[triu_ixs], size=int(diff))

        target_g[(triu_ixs[0][extra_edges], triu_ixs[1][extra_edges])] = 1
        target_g[(triu_ixs[1][extra_edges], triu_ixs[0][extra_edges])] = 1

    target_g = symmetric(target_g)
    return target_g


def score_matrix_from_random_walks(random_walks, N, symmetric=True):
    random_walks = np.array(random_walks)
    bigrams = np.array(list(zip(random_walks[:, :-1], random_walks[:, 1:])))
    bigrams = np.transpose(bigrams, [0, 2, 1])
    bigrams = bigrams.reshape([-1, 2])
    if symmetric:
        bigrams = np.row_stack((bigrams, bigrams[:, ::-1]))

    mat = sp.coo_matrix((np.ones(bigrams.shape[0]), (bigrams[:, 0], bigrams[:, 1])), shape=[N, N])
    return mat


def symmetric(directed_adjacency, clip_to_one=True):
    A_symmetric = directed_adjacency + directed_adjacency.T
    if clip_to_one:
        A_symmetric[A_symmetric > 1] = 1
    return A_symmetric


def degrees(A_in):
    degrees = A_in.sum(axis=0)
    return np.max(degrees), np.min(degrees), np.mean(degrees)


def statistics_LCC(A_in):
    unique, counts = np.unique(connected_components(A_in)[1], return_counts=True)
    LCC = np.where(connected_components(A_in)[1] == np.argmax(counts))[0]
    return LCC


def wedge_count(A_in):
    degrees = A_in.sum(axis=0)
    return float(np.sum(np.array([0.5 * x * (x - 1) for x in degrees])))


def claw_count(A_in):
    degrees = A_in.sum(axis=0)
    return float(np.sum(np.array([1 / 6. * x * (x - 1) * (x - 2) for x in degrees])))


def triangle_count(A_in):
    A_graph = nx.from_numpy_matrix(A_in)
    triangles = nx.triangles(A_graph)
    t = np.sum(list(triangles.values())) / 3
    return int(t)


def gini(A_in):
    n = A_in.shape[0]
    degrees = A_in.sum(axis=0)
    degrees_sorted = np.sort(degrees)
    G = (2 * np.sum(np.array([i * degrees_sorted[i] for i in range(len(degrees))]))) / (n * np.sum(degrees)) - (
            n + 1) / n
    return float(G)


def edge_distribution_entropy(A_in):
    degrees = A_in.sum(axis=0)
    m = 0.5 * np.sum(np.square(A_in))
    n = A_in.shape[0]

    H_er = 1 / np.log(n) * np.sum(-degrees / (2 * float(m)) * np.log((degrees + .0001) / (2 * float(m))))
    return H_er

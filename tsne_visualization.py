from dataset import get_polaroid_dataset
from graph_convolution import GraphConvolutionNetwork
from postprocessing import visualize_data_embedding
from trainer import TransductiveTrainer

dataset_name = 'CiteSeer'
dataset = get_polaroid_dataset(dataset_name)
data = dataset.data

model = GraphConvolutionNetwork(layers_size=[dataset.num_features, 32, dataset.num_classes])
trainer = TransductiveTrainer(model, learning_rate=0.005, weight_decay=5e-5)
loss, train_accuracy, _ = trainer.train(data, number_of_epochs=125, max_patience=100)

embeddings = {
    dataset_name + ' - 2D reprezentacija': data.x.numpy(),
    dataset_name + ' - 2D reprezentacija izlaza 1. sloja': model.layer_outputs[-2],
    dataset_name + ' - 2D reprezentacija izlaza 2. sloja': model.layer_outputs[-1]
}

visualize_data_embedding(embeddings, data.y.numpy())

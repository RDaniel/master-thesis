import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import seaborn as sns

sns.set(palette='colorblind', color_codes=False)
sns.set_context(font_scale=1.25, rc={"lines.linewidth": 2})
sns.set_style({'axes.grid': False})


def show_graph(matrix, title):
    plt.imshow(matrix, interpolation='none', cmap='cividis')
    color_bar = plt.colorbar()
    color_bar.ax.tick_params(labelsize=14)
    plt.title(title, fontsize=15)
    plt.show()


def normalize(matrix):
    return (matrix - matrix.min()) / (matrix.max() - matrix.min())


# Line graph
A = np.asarray([[0, 1, 0, 0, 0], [1, 0, 1, 0, 0], [0, 1, 0, 1, 0], [0, 0, 1, 0, 1], [0, 0, 0, 1, 0]])

graph = nx.from_numpy_matrix(np.asmatrix(A))
nx.draw(graph, node_size=1000, font_size=20, node_color='#65B9F3', edge_color='#C4BBBA', with_labels=True)
plt.show()

N = A.shape[0]
D = np.sum(A, 0)
D_hat = np.diag((D + 1e-5) ** (-0.5))
L = np.identity(N) - np.dot(D_hat, A).dot(D_hat)
show_graph(A, 'A')
show_graph(np.matmul(A, A), r'$A^{2}$')
show_graph(L, r'L')
show_graph(np.matmul(L, L), r'$L^{2}$')

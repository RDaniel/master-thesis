import torch
import torch.nn.functional as F
from torch_geometric.nn import GraphConv, global_mean_pool, global_max_pool


class GraphConvolutionNetwork(torch.nn.Module):

    def __init__(self, layers_size):
        super(GraphConvolutionNetwork, self).__init__()

        self.layers = torch.nn.ModuleList()
        self.layer_outputs = []

        for i in range(len(layers_size) - 1):
            conv = GraphConv(layers_size[i], layers_size[i + 1])
            self.layers.append(conv)

    def forward(self, data, logits=False):
        self.layer_outputs.clear()
        x, edge_index, edge_weight = data.x, data.edge_index, data.edge_attr

        for layer in self.layers:
            x = F.dropout(x, p=0.5, training=self.training)
            x = layer(x, edge_index, edge_weight)
            self.layer_outputs.append(x)

            if layer is not self.layers[-1]:
                x = F.relu(x)

        if logits:
            return x
        else:
            return F.log_softmax(x, dim=1)


class GraphConvolutionPoolingNetwork(torch.nn.Module):

    def __init__(self, input, output, hidden=128):
        super(GraphConvolutionPoolingNetwork, self).__init__()

        self.conv1 = GraphConv(input, hidden)
        self.conv2 = GraphConv(hidden, hidden)
        self.conv3 = GraphConv(hidden, hidden)

        self.lin1 = torch.nn.Linear(hidden * 2 * 3, hidden)
        self.lin2 = torch.nn.Linear(hidden, 128)
        self.lin3 = torch.nn.Linear(128, output)

    def forward(self, data, logits=False):
        x, edge_index, edge_weight, batch = data.x, data.edge_index, data.edge_attr, data.batch

        x = F.relu(self.conv1(x, edge_index, edge_weight))
        x1 = torch.cat([global_mean_pool(x, batch), global_max_pool(x, batch)], dim=1)

        x = F.relu(self.conv2(x, edge_index, edge_weight))
        x2 = torch.cat([global_mean_pool(x, batch), global_max_pool(x, batch)], dim=1)

        x = F.relu(self.conv3(x, edge_index, edge_weight))
        x3 = torch.cat([global_mean_pool(x, batch), global_max_pool(x, batch)], dim=1)

        x = torch.cat([x1, x2, x3], dim=1)

        x = F.relu(self.lin1(x))
        x = F.dropout(x, p=0.5, training=self.training)
        x = F.relu(self.lin2(x))

        if logits:
            return x
        else:
            return F.log_softmax(self.lin3(x), dim=-1)
